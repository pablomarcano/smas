$(document).ready(function(){
	/**
	 * Funcion del carrusel principal
	 */
	$('#principal-carousel').slick();


	/**
	 * Funcion del carrusel de categorias
	 */
	$('#categories-carousel').slick({
	  infinite: true,
	  slidesToShow: 5,
	  slidesToScroll: 1,
	  centerMode: true,
  	  centerPadding: '10px',
  	  autoplay:true,
      arrows: true,
      prevArrow:"<img src='../img/categories-carousel/prev.png' class='arrowsCarousel' id='prevArrow'>",
      nextArrow:"<img src='../img/categories-carousel/next.png' class='arrowsCarousel' id='nextArrow'>"
	});
})